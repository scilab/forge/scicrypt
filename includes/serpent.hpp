//***********************************************************************
// Vincent COUVERT							
// Florent LEBEAU							
// DIGITEO 2009								
// SciCrypt Toolbox							
// This file is released under the terms of the CeCILL license.		
//***********************************************************************

#ifndef _SERPENT_HPP
#define _SERPENT_HPP

/*************************** Include C++ ****************************/
#include <iostream>
using namespace std;

/************************* Include Crypto++ *************************/
#include "cryptopp/osrng.h"
using CryptoPP::AutoSeededRandomPool;

#include "cryptopp/files.h"
using CryptoPP::FileSink;
using CryptoPP::FileSource;

#include "cryptopp/cryptlib.h"
using namespace CryptoPP;

#include "cryptopp/serpent.h"
using CryptoPP::Serpent;

#include "cryptopp/modes.h"
using CryptoPP::OFB_Mode;
using CryptoPP::CBC_Mode;
using CryptoPP::CFB_Mode;

#include "cryptopp/rsa.h"
using CryptoPP::RSA;

/************************* Include SciCrypt *************************/
#include "rsa.hpp"

/********************************************************************/

class serpent_cbc : public rsa
{
	public:
		serpent_cbc();
		string enc(string);
		bool encryption(const string&, const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&, const string&);
		bool KeysGenerator();
		~serpent_cbc();
};

class serpent_ofb : public rsa
{
	public:
		serpent_ofb();
		string enc(string);
		bool encryption(const string&, const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&, const string&);
		bool KeysGenerator();
		~serpent_ofb();
};

class serpent_cfb : public rsa
{
	public:
		serpent_cfb();
		string enc(string);
		bool encryption(const string&, const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&, const string&);
		bool KeysGenerator();
		~serpent_cfb();
};

class serpent_ecb : public rsa
{
	public:
		serpent_ecb();
		string enc(string);
		bool encryption(const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&);
		bool KeysGenerator();
		~serpent_ecb();
};

class serpent_ctr : public rsa
{
	public:
		serpent_ctr();
		string enc(string);
		bool encryption(const string&, const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&, const string&);
		bool KeysGenerator();
		~serpent_ctr();
};

#endif 


