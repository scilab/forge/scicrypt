//***********************************************************************
// Vincent COUVERT
// Florent LEBEAU
// DIGITEO 2009
// SciCrypt Toolbox
// This file is released under the terms of the CeCILL license.
//***********************************************************************

#ifndef _CPHR_HPP
#define _CPHR_HPP

/*************************** Include C++ ****************************/
#include <iostream>
using namespace std;

/************************* Include Crypto++ *************************/
#include "cryptopp/osrng.h"
using CryptoPP::AutoSeededRandomPool;

#include "cryptopp/hex.h"
using CryptoPP::HexEncoder;
using CryptoPP::HexDecoder;

#include "cryptopp/files.h"
using CryptoPP::FileSink;
using CryptoPP::FileSource;

#include "cryptopp/cryptlib.h"
using namespace CryptoPP;

#include "cryptopp/aes.h"
using CryptoPP::AES;
#include "cryptopp/serpent.h"
using CryptoPP::Serpent;
#include "cryptopp/des.h"
using CryptoPP::DES_EDE3;
#include "cryptopp/twofish.h"
using CryptoPP::Twofish;
#include "cryptopp/cast.h"
using CryptoPP::CAST256;
#include "cryptopp/blowfish.h"
using CryptoPP::Blowfish;

/********************************************************************/

typedef enum
{
	AES128, TDES, SERPENT, TWFSH, CST, BLWFSH
} CIPH; // Symmetric cipher algorithms identifers

typedef enum
{
	CBC, OFB, CFB, ECB, CTR
} MODE; // Cipher modes identifiers


class cphr
{
	protected:
		CIPH algo;
		string nameAlgo;
		MODE mode;
		string nameMode;
		int keylength;
		int blocksize;
		byte* key;	// symmetric key
		byte* iv;	// initialization vector (IV)
		string outFilename; // name of the output file created
	public:
		cphr(CIPH, MODE);
		bool setIV(string);
		bool setKey(string);
		bool setKeyIV(string, string);
		bool setOutFn(string);
		bool genIV();
		bool genKey();
		bool genKeyIV();
		string getIV() const;
		string getKey() const;
		string getOutFn() const;
		bool dspIV() const;
		bool dspKey() const;
		bool dspAll() const;
		bool saveIV(const string&) const;
		bool saveKey(const string&) const;
		bool saveKeyIV(const string&, const string&) const;
		bool loadIV(const string&);
		bool loadKey(const string&);
		bool loadKeyIV(const string&, const string&);
		string readF(const string&) const;
		string readCphrF(const string&) const;
		bool createF(string, const string&) const;
		bool createCphrF(string, const string&) const;
		~cphr();
};

int chkFiles(char*, char*);

#endif
