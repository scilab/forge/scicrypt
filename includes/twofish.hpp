//***********************************************************************
// Vincent COUVERT							
// Florent LEBEAU							
// DIGITEO 2009								
// SciCrypt Toolbox							
// This file is released under the terms of the CeCILL license.		
//***********************************************************************

#ifndef _TWOFISH_HPP
#define _TWOFISH_HPP

/*************************** Include C++ ****************************/
#include <iostream>
using namespace std;

/************************* Include Crypto++ *************************/
#include "cryptopp/osrng.h"
using CryptoPP::AutoSeededRandomPool;

#include "cryptopp/files.h"
using CryptoPP::FileSink;
using CryptoPP::FileSource;

#include "cryptopp/cryptlib.h"
using namespace CryptoPP;

#include "cryptopp/twofish.h"
using CryptoPP::Twofish;

#include "cryptopp/modes.h"
using CryptoPP::OFB_Mode;
using CryptoPP::CBC_Mode;
using CryptoPP::CFB_Mode;

#include "cryptopp/rsa.h"
using CryptoPP::RSA;

/************************* Include SciCrypt *************************/
#include "rsa.hpp"

/********************************************************************/

class twofish_cbc : public rsa
{
	public:
		twofish_cbc();
		string enc(string);
		bool encryption(const string&, const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&, const string&);
		bool KeysGenerator();
		~twofish_cbc();
};

class twofish_ofb : public rsa
{
	public:
		twofish_ofb();
		string enc(string);
		bool encryption(const string&, const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&, const string&);
		bool KeysGenerator();
		~twofish_ofb();
};

class twofish_cfb : public rsa
{
	public:
		twofish_cfb();
		string enc(string);
		bool encryption(const string&, const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&, const string&);
		bool KeysGenerator();
		~twofish_cfb();
};

class twofish_ecb : public rsa
{
	public:
		twofish_ecb();
		string enc(string);
		bool encryption(const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&);
		bool KeysGenerator();
		~twofish_ecb();
};

class twofish_ctr : public rsa
{
	public:
		twofish_ctr();
		string enc(string);
		bool encryption(const string&, const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&, const string&);
		bool KeysGenerator();
		~twofish_ctr();
};

#endif 


