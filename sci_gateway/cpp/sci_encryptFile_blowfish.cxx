//***********************************************************************
// Vincent COUVERT
// Florent LEBEAU
// DIGITEO 2009
// SciCrypt Toolbox
// This file is released under the terms of the CeCILL license.
//***********************************************************************
#include "functions.hpp"
/* ==================================================================== */
extern "C"
{

#include <stdio.h>
#include <string.h>
/* ==================================================================== */
#define __USE_DEPRECATED_STACK_FUNCTIONS__
#include "stack-c.h"
#include "Scierror.h"
#include "sciprint.h"
/* ==================================================================== */

int sci_encryptFile_blowfish(char *fname)
{
	int l1, m1, n1, l2, m2, n2, m3, n3, l3, m4, n4, l4, m5, n5, l5, m6, n6, l6;

	char *plain, *pKey, *key, *iv, *outFn;

	FILE *plainF, *pKeyF, *keyF, *ivF;

	int mode;

	plain = NULL;
	pKey = NULL;
	key = NULL;
	iv = NULL;

	/* check that we have only 6 input parameters */
	/* check that we have only 1 output parameters */
	CheckRhs(6,6) ;
	CheckLhs(1,1) ;

	/* get first parameter and put in 'plainF' */
	GetRhsVar(1, STRING_DATATYPE, &m1, &n1, &l1);
	plain = cstk(l1) ;

	/* get second parameter and put in 'pKey' */
	GetRhsVar(2, STRING_DATATYPE, &m2, &n2, &l2);
	pKey = cstk(l2);

	/* get third parameter and put in 'key' */
	GetRhsVar(3, STRING_DATATYPE, &m3, &n3, &l3);
	key = cstk(l3) ;

	/* get fourth parameter and put in 'mode' if different than ECB if mode is not precised, use ECB mode */
	GetRhsVar(4, STRING_DATATYPE, &m4, &n4, &l4);
	if(strcmp(cstk(l4), "") ==  0)
	{
	    mode = 0;
	}
	else
    {
        mode = atoi(cstk(l4)) ;
    }

	/* get fifth parameter and put in 'iv' */
	GetRhsVar(5, STRING_DATATYPE, &m5, &n5, &l5);
	iv = cstk(l5) ;

	/* get sixth parameter and put in 'outFn' if not precised, use "cipher" */
	GetRhsVar(6, STRING_DATATYPE, &m6, &n6, &l6);
	outFn = cstk(l6) ;
	if(strcmp(outFn, "") ==  0)
	{
	    outFn = "cipher";
	}

	/* check parameters correctness */
	plainF = fopen(plain, "r");
	if(plainF == NULL)
	{
		Scierror(999,"%s: Input file #%d doesn't exist or can't be read: please enter an appropriate file.\n",fname,1);
		return 0;
	}
	fclose(plainF);
	pKeyF = fopen(pKey, "r");
	if(pKeyF ==  NULL)
	{
		Scierror(999,"%s: Input file #%d doesn't exist or can't be read: please enter an appropriate file.\n",fname,2);
		return 0;
	}
	fclose(pKeyF);
	keyF = fopen(key, "r");
	if(keyF == NULL)
	{
		Scierror(999,"%s: Input file #%d doesn't exist or can't be read: please enter an appropriate file.\n",fname,3);
		return 0;
	}
	fclose(keyF);
	if(mode < 0 || mode > 4)
	{
		Scierror(999,"%s: Input integer #%d must be between 0 and 4: please check help for details.\n",fname,4);
		return 0;
	}
	/* open 'iv' file only if 'mode' is different than ECB */
	if(mode != 0)
	{
	    ivF = fopen(iv, "r");
        if(ivF == NULL)
        {
            Scierror(999,"%s: Input file #%d doesn't exist or can't be read: please enter an appropriate file.\n",fname,5);
            return 0;
        }
        fclose(ivF);
	}

	/* call encryptFile_blowfish subroutine */
	if(encryptFile_blowfish(plain, pKey, key, mode, iv, outFn))
	{
		Scierror(999,"%s: Input file #%d can't be encrypted in the output folder #%d. Please check permissions.\n", fname, 1, 6);
		return 0;
	}


	return 0;
}
/* ==================================================================== */
} // extern C
/* ==================================================================== */
