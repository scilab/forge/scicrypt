//***********************************************************************
// Vincent COUVERT
// Florent LEBEAU
// DIGITEO 2009
// SciCrypt Toolbox
// This file is released under the terms of the CeCILL license.
//***********************************************************************
#include "functions.hpp"
/* ==================================================================== */
extern "C"
{
/* ==================================================================== */
#define __USE_DEPRECATED_STACK_FUNCTIONS__
#include "stack-c.h"
#include "Scierror.h"
#include "sciprint.h"
/* ==================================================================== */

int sci_keyGen_tdes(char *fname)
{
	int l1, m1, n1, l2, m2, n2, m3, n3,l3, m4, n4,l4;

	char *key, *iv, *pKey, *sKey;

	key = NULL;
	iv = NULL;
	pKey = NULL;
	sKey = NULL;

	/* check that we have only 4 parameters input */
	/* check that we have only 0 parameters output */
	CheckRhs(4,4) ;
	CheckLhs(1,1) ;

	/* get first parameter and put in 'plainF' */
	GetRhsVar(1, STRING_DATATYPE, &m1, &n1, &l1);
	key = cstk(l1) ;

	/* get second parameter and put in 'pKey' */
	GetRhsVar(2, STRING_DATATYPE, &m2, &n2, &l2);
	iv = cstk(l2);

	/* get third parameter and put in 'key' */
	GetRhsVar(3, STRING_DATATYPE, &m3, &n3, &l3);
	pKey = cstk(l3) ;

	/* get fourth parameter and put in 'iv' */
	GetRhsVar(4, STRING_DATATYPE, &m4, &n4, &l4);
	sKey = cstk(l4) ;

	/* call keyGen_tdes subroutine */
	if(keyGen_tdes(key, iv, pKey, sKey))
	{
		Scierror(999,"%s: Files can't be created in the current folder. Please check permissions.\n", fname);
		return 0;
	}

	sciprint("\nDone\n");

	return 0;
}
/* ==================================================================== */
} // extern C
/* ==================================================================== */
