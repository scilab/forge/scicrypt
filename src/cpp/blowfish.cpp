//***********************************************************************
// Vincent COUVERT							
// Florent LEBEAU							
// DIGITEO 2009								
// SciCrypt Toolbox							
// This file is released under the terms of the CeCILL license.		
//***********************************************************************

#include "blowfish.hpp"


/*
 * name:	blowfish, class constructor 
 * @param	symmetric cipher CPHR, cipher mode MODE
 * @return	
 */

blowfish_cbc::blowfish_cbc() : rsa(BLWFSH, CBC) {}

/*
 * name:	enc, encrypts from string using default parameters (key etc) 
 * @param	plain
 * @return	ciphered result
 */

string blowfish_cbc::enc(string plain)
{
	string cipher;
	    
	try
	{	
		CBC_Mode< Blowfish >::Encryption e;
		
		cout << e.AlgorithmName() << " encryption" << endl;
		
		e.SetKeyWithIV( key, keylength, iv );

		StringSource( plain, true, 
			new StreamTransformationFilter( e,
				new StringSink( cipher )
			) // StreamTransformationFilter      
		); // StringSource		
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

	return cipher;
}

/*
 * name:	encryption, encrypts from file and sets parameters (key etc),
 * 			it creates a symmetric ciphered file "cipher" and 
 * 			an RSA ciphered symmetric key "cipheredKey"
 * @param	pKey, public RSA key file
 * 			key, symmetric key file, which will be RSA encoded
 * 			iv, IV file
 * 			plainF, input file to cipher
 * @return	0 for success, 1 for failure
 */

bool blowfish_cbc::encryption(const string& pKey, const string& key, const string& iv, const string& plainF)
{	
	if(loadKeyIV(key, iv))
	{
		return EXIT_FAILURE;
	}			     
	
	//////////////////////////Encoding
	string plain, cipher;
	plain = readF(plainF);
	cipher = enc(plain);
	if(createCphrF(cipher, outFilename))
	{
		cout << "ERROR creating encoded file" << endl;
		return EXIT_FAILURE;
	}
	
	//////////////////////////RSA encoding
	load_pubKey(pKey);
	string cipherKey1 = rsa_enc(getKey());
	if(createF(cipherKey1, "cipherKey"))
	{
		cout << "ERROR creating encoded key file" << endl;
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

/*
 * name:	dec, decrypts from string using default parameters (key etc) 
 * @param	plain
 * @return	deciphered result
 */

string blowfish_cbc::dec(string cipher)
{
	string recovered;

	try
	{		
		CBC_Mode< Blowfish >::Decryption d;
	
		cout << d.AlgorithmName() << " decryption" << endl;
	
		d.SetKeyWithIV( key, keylength, iv );

		StringSource( cipher, true, 
			new StreamTransformationFilter( d,
				new StringSink( recovered )
			) // StreamTransformationFilter
		); // StringSource
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

	return recovered;
}

/*
 * name:	decryption, decrypts from file and sets parameters (key etc),
 * 			it creates a deciphered file 
 * @param	sKey, private/secret RSA key file
 * 			cipherKeyF, RSA encoded symmetric key file
 * 			iv, IV file
 * 			cipherF, input file to decipher
 * @return	0 for success, 1 for failure
 */

bool blowfish_cbc::decryption(const string& sKey, const string& cipherKeyF, const string& iv, const string& cipherF)
{	
	if(loadIV(iv))
	{
		cout << "ERROR setting IV" << endl;
		return EXIT_FAILURE;
	}
	//////////////////////////RSA decoding
	load_privKey(sKey);
	string cipherKey = readCphrF(cipherKeyF);
	string recoveredKey = rsa_dec(cipherKey);
	if(setKey(recoveredKey))
	{
		cout << "ERROR loading recovered key" << endl;
		return EXIT_FAILURE;
	}
	
	//////////////////////////Decoding
	string cipher = readCphrF(cipherF);
	string recovered = dec(cipher);
	if(createF(recovered, outFilename))
	{
		cout << "ERROR creating decoded file" << endl;
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

bool blowfish_cbc::KeysGenerator()
{
	if(genKeyIV())
	{
		return EXIT_FAILURE;
	}
	if(rsaGenKeys())
	{
		return EXIT_FAILURE;
	}
	saveKeyIV("key", "iv");
	if(save_pubKey("pKey"))
	{
		return EXIT_FAILURE;
	}
	if(save_privKey("sKey"))
	{
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

blowfish_cbc::~blowfish_cbc(){}

/***************************************************************************/

blowfish_ofb::blowfish_ofb() : rsa(BLWFSH, OFB) {}

string blowfish_ofb::enc(string plain)
{
	string cipher;
	    
	try
	{	
		OFB_Mode< Blowfish >::Encryption e;
		
		cout << e.AlgorithmName() << " encryption" << endl;
		
		e.SetKeyWithIV( key, keylength, iv );

		StringSource( plain, true, 
			new StreamTransformationFilter( e,
				new StringSink( cipher )
			) // StreamTransformationFilter      
		); // StringSource		
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

	return cipher;
}

bool blowfish_ofb::encryption(const string& pKey, const string& key, const string& iv, const string& plainF)
{
	if(loadKeyIV(key, iv))
	{
		return EXIT_FAILURE;
	}			     
	
	//////////////////////////Encoding
	string plain, cipher;
	plain = readF(plainF);
	cipher = enc(plain);
	if(createCphrF(cipher, outFilename))
	{
		cout << "ERROR creating encoded file" << endl;
		return EXIT_FAILURE;
	}
	
	//////////////////////////RSA encoding
	load_pubKey(pKey);
	string cipherKey1 = rsa_enc(getKey());
	if(createF(cipherKey1, "cipherKey"))
	{
		cout << "ERROR creating encoded key file" << endl;
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

string blowfish_ofb::dec(string cipher)
{
	string recovered;
	
	try
	{		
		OFB_Mode< Blowfish >::Decryption d;
		
		cout << d.AlgorithmName() << " decryption" << endl;
		
		d.SetKeyWithIV( key, keylength, iv );

		StringSource( cipher, true, 
			new StreamTransformationFilter( d,
				new StringSink( recovered )
			) // StreamTransformationFilter
		); // StringSource
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

    return recovered;
}

bool blowfish_ofb::decryption(const string& sKey, const string& cipherKeyF, const string& iv, const string& cipherF)
{
	if(loadIV(iv))
	{
		cout << "ERROR setting IV" << endl;
		return EXIT_FAILURE;
	}
	//////////////////////////RSA decoding
	load_privKey(sKey);
	string cipherKey = readCphrF(cipherKeyF);
	string recoveredKey = rsa_dec(cipherKey);
	if(setKey(recoveredKey))
	{
		cout << "ERROR loading recovered key" << endl;
		return EXIT_FAILURE;
	}
	
	//////////////////////////Decoding
	string cipher = readCphrF(cipherF);
	string recovered = dec(cipher);
	if(createF(recovered, outFilename))
	{
		cout << "ERROR creating decoded file" << endl;
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

bool blowfish_ofb::KeysGenerator()
{
	if(genKeyIV())
	{
		return EXIT_FAILURE;
	}
	if(rsaGenKeys())
	{
		return EXIT_FAILURE;
	}
	saveKeyIV("key", "iv");
	if(save_pubKey("pKey"))
	{
		return EXIT_FAILURE;
	}
	if(save_privKey("sKey"))
	{
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

blowfish_ofb::~blowfish_ofb(){}

/***************************************************************************/

blowfish_cfb::blowfish_cfb() : rsa(BLWFSH, CFB) {}

string blowfish_cfb::enc(string plain)
{
	string cipher;
	    
	try
	{	
		CFB_Mode< Blowfish >::Encryption e;
		
		cout << e.AlgorithmName() << " encryption" << endl;
		
		e.SetKeyWithIV( key, keylength, iv );

		StringSource( plain, true, 
			new StreamTransformationFilter( e,
				new StringSink( cipher )
			) // StreamTransformationFilter      
		); // StringSource		
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

	return cipher;
}

bool blowfish_cfb::encryption(const string& pKey, const string& key, const string& iv, const string& plainF)
{
	if(loadKeyIV(key, iv))
	{
		return EXIT_FAILURE;
	}			     
	
	//////////////////////////Encoding
	string plain, cipher;
	plain = readF(plainF);
	cipher = enc(plain);
	if(createCphrF(cipher, outFilename))
	{
		cout << "ERROR creating encoded file" << endl;
		return EXIT_FAILURE;
	}
	
	//////////////////////////RSA encoding
	load_pubKey(pKey);
	string cipherKey1 = rsa_enc(getKey());
	if(createF(cipherKey1, "cipherKey"))
	{
		cout << "ERROR creating encoded key file" << endl;
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

string blowfish_cfb::dec(string cipher)
{
	string recovered;
	
	try
	{		
		CFB_Mode< Blowfish >::Decryption d;
		
		cout << d.AlgorithmName() << " decryption" << endl;
		
		d.SetKeyWithIV( key, keylength, iv );

		StringSource( cipher, true, 
			new StreamTransformationFilter( d,
				new StringSink( recovered )
			) // StreamTransformationFilter
		); // StringSource
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

    return recovered;
}

bool blowfish_cfb::decryption(const string& sKey, const string& cipherKeyF, const string& iv, const string& cipherF)
{
	if(loadIV(iv))
	{
		cout << "ERROR setting IV" << endl;
		return EXIT_FAILURE;
	}
	//////////////////////////RSA decoding
	load_privKey(sKey);
	string cipherKey = readCphrF(cipherKeyF);
	string recoveredKey = rsa_dec(cipherKey);
	if(setKey(recoveredKey))
	{
		cout << "ERROR loading recovered key" << endl;
		return EXIT_FAILURE;
	}
	
	//////////////////////////Decoding
	string cipher = readCphrF(cipherF);
	string recovered = dec(cipher);
	if(createF(recovered, outFilename))
	{
		cout << "ERROR creating decoded file" << endl;
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

bool blowfish_cfb::KeysGenerator()
{
	if(genKeyIV())
	{
		return EXIT_FAILURE;
	}
	if(rsaGenKeys())
	{
		return EXIT_FAILURE;
	}
	saveKeyIV("key", "iv");
	if(save_pubKey("pKey"))
	{
		return EXIT_FAILURE;
	}
	if(save_privKey("sKey"))
	{
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

blowfish_cfb::~blowfish_cfb(){}

/***************************************************************************/

blowfish_ecb::blowfish_ecb() : rsa(BLWFSH, ECB) {}

string blowfish_ecb::enc(string plain)
{
	string cipher;
	    
	try
	{	
		ECB_Mode< Blowfish >::Encryption e;
		
		cout << e.AlgorithmName() << " encryption" << endl;
		
		e.SetKey( key, keylength );

		StringSource( plain, true, 
			new StreamTransformationFilter( e,
				new StringSink( cipher )
			) // StreamTransformationFilter      
		); // StringSource		
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

	return cipher;
}

bool blowfish_ecb::encryption(const string& pKey, const string& key, const string& plainF)
{
	if(loadKey(key))
	{
		return EXIT_FAILURE;
	}			     
	
	//////////////////////////Encoding
	string plain, cipher;
	plain = readF(plainF);
	cipher = enc(plain);
	if(createCphrF(cipher, outFilename))
	{
		cout << "ERROR creating encoded file" << endl;
		return EXIT_FAILURE;
	}
	
	//////////////////////////RSA encoding
	load_pubKey(pKey);
	string cipherKey1 = rsa_enc(getKey());
	if(createF(cipherKey1, "cipherKey"))
	{
		cout << "ERROR creating encoded key file" << endl;
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

string blowfish_ecb::dec(string cipher)
{
	string recovered;
	
	try
	{		
		ECB_Mode< Blowfish >::Decryption d;
		
		cout << d.AlgorithmName() << " decryption" << endl;
		
		d.SetKey( key, keylength );

		StringSource( cipher, true, 
			new StreamTransformationFilter( d,
				new StringSink( recovered )
			) // StreamTransformationFilter
		); // StringSource
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

    return recovered;
}

bool blowfish_ecb::decryption(const string& sKey, const string& cipherKeyF, const string& cipherF)
{
	//////////////////////////RSA decoding
	load_privKey(sKey);
	string cipherKey = readCphrF(cipherKeyF);
	string recoveredKey = rsa_dec(cipherKey);
	if(setKey(recoveredKey))
	{
		cout << "ERROR loading recovered key" << endl;
		return EXIT_FAILURE;
	}
	
	//////////////////////////Decoding
	string cipher = readCphrF(cipherF);
	string recovered = dec(cipher);
	if(createF(recovered, outFilename))
	{
		cout << "ERROR creating decoded file" << endl;
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

bool blowfish_ecb::KeysGenerator()
{
	if(genKeyIV())
	{
		return EXIT_FAILURE;
	}
	if(rsaGenKeys())
	{
		return EXIT_FAILURE;
	}
	saveKeyIV("key", "iv");
	if(save_pubKey("pKey"))
	{
		return EXIT_FAILURE;
	}
	if(save_privKey("sKey"))
	{
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

blowfish_ecb::~blowfish_ecb(){}

/***************************************************************************/

blowfish_ctr::blowfish_ctr() : rsa(BLWFSH, CTR) {}

string blowfish_ctr::enc(string plain)
{
	string cipher;
	    
	try
	{	
		CTR_Mode< Blowfish >::Encryption e;
		
		cout << e.AlgorithmName() << " encryption" << endl;
		
		e.SetKeyWithIV( key, keylength, iv );

		StringSource( plain, true, 
			new StreamTransformationFilter( e,
				new StringSink( cipher )
			) // StreamTransformationFilter      
		); // StringSource		
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

	return cipher;
}

bool blowfish_ctr::encryption(const string& pKey, const string& key, const string& iv, const string& plainF)
{
	if(loadKeyIV(key, iv))
	{
		return EXIT_FAILURE;
	}			     
	
	//////////////////////////Encoding
	string plain, cipher;
	plain = readF(plainF);
	cipher = enc(plain);
	if(createCphrF(cipher, outFilename))
	{
		cout << "ERROR creating encoded file" << endl;
		return EXIT_FAILURE;
	}
	
	//////////////////////////RSA encoding
	load_pubKey(pKey);
	string cipherKey1 = rsa_enc(getKey());
	if(createF(cipherKey1, "cipherKey"))
	{
		cout << "ERROR creating encoded key file" << endl;
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

string blowfish_ctr::dec(string cipher)
{
	string recovered;
	
	try
	{		
		CTR_Mode< Blowfish >::Decryption d;
		
		cout << d.AlgorithmName() << " decryption" << endl;
		
		d.SetKeyWithIV( key, keylength, iv );

		StringSource( cipher, true, 
			new StreamTransformationFilter( d,
				new StringSink( recovered )
			) // StreamTransformationFilter
		); // StringSource
	}
	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
		cerr << endl;
	}

    return recovered;
}

bool blowfish_ctr::decryption(const string& sKey, const string& cipherKeyF, const string& iv, const string& cipherF)
{
	if(loadIV(iv))
	{
		cout << "ERROR setting IV" << endl;
		return EXIT_FAILURE;
	}
	//////////////////////////RSA decoding
	load_privKey(sKey);
	string cipherKey = readCphrF(cipherKeyF);
	string recoveredKey = rsa_dec(cipherKey);
	if(setKey(recoveredKey))
	{
		cout << "ERROR loading recovered key" << endl;
		return EXIT_FAILURE;
	}
	
	//////////////////////////Decoding
	string cipher = readCphrF(cipherF);
	string recovered = dec(cipher);
	if(createF(recovered, outFilename))
	{
		cout << "ERROR creating decoded file" << endl;
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

bool blowfish_ctr::KeysGenerator()
{
	if(genKeyIV())
	{
		return EXIT_FAILURE;
	}
	if(rsaGenKeys())
	{
		return EXIT_FAILURE;
	}
	saveKeyIV("key", "iv");
	if(save_pubKey("pKey"))
	{
		return EXIT_FAILURE;
	}
	if(save_privKey("sKey"))
	{
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

blowfish_ctr::~blowfish_ctr(){}
