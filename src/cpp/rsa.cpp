//***********************************************************************
// Vincent COUVERT
// Florent LEBEAU
// DIGITEO 2009
// SciCrypt Toolbox
// This file is released under the terms of the CeCILL license.
//***********************************************************************

#include "rsa.hpp"


/*
 *
 * name:	rsa, class constructor
 * @param	symmetric cipher CPHR, cipher mode MODE
 * @return
 */

rsa::rsa(CIPH c, MODE m) : cphr(c, m) {}

/*
 *
 * name:	rsaGenKeys, random RSA keys generation
 * @param
 * @return	0 for success, 1 for failure
 */

bool rsa::rsaGenKeys()
{
	AutoSeededRandomPool rng;

	parameters.GenerateRandomWithKeySize( rng, 1024 );
	RSA::PrivateKey sKey( parameters );
	RSA::PublicKey pKey( parameters );

	privateKey = sKey;
	publicKey = pKey;

	return EXIT_SUCCESS;
}

/*
 *
 * name:	save_pubKey, saves public RSA key into a file
 * @param	filename
 * @return	0 for success, 1 for failure
 */

bool rsa::save_pubKey(const string& filename) const
{
	// DER Encode Key - X.509 key format
	publicKey.Save(
		FileSink( filename.c_str(), true /*binary*/ ).Ref()
	);
	return EXIT_SUCCESS;
}

/*
 *
 * name:	save_privKey, saves private RSA key into a file
 * @param	filename
 * @return	0 for success, 1 for failure
 */

bool rsa::save_privKey(const string& filename) const
{
	// DER Encode Key - PKCS #8 key format
	privateKey.Save(
		FileSink( filename.c_str(), true /*binary*/ ).Ref()
	);
	return EXIT_SUCCESS;
}

/*
 *
 * name:	load_pubKey, load public RSA key from a file
 * @param	filename
 * @return	0 for success, 1 for failure
 */

bool rsa::load_pubKey(const string& filename)
{
	// DER Encode Key - X.509 key format
	publicKey.Load(
		FileSource( filename.c_str(), true, NULL, true /*binary*/ ).Ref()
	);
	return EXIT_SUCCESS;
}

/*
 *
 * name:	load_privKey, load private RSA key from a file
 * @param	filename
 * @return	0 for success, 1 for failure
 */

bool rsa::load_privKey(const string& filename)
{
	// DER Encode Key - PKCS #8 key format
	privateKey.Load(
		FileSource( filename.c_str(), true, NULL, true /*binary*/ ).Ref()
	);
	return EXIT_SUCCESS;
}

/*
 *
 * name:	rsa_enc, RSA encryption using public key
 * @param	plain
 * @return	ciphered result
 */

string rsa::rsa_enc(string plain) const
{
	if(key == NULL)
	{
		return NULL;
	}

	string cipher;
	try
	{
		AutoSeededRandomPool rng;
		RSAES_PKCS1v15_Encryptor e( publicKey );

	        StringSource( plain, true,
			new PK_EncryptorFilter( rng, e,
				new HexEncoder(
					new StringSink( cipher )
				) // HexEncoder
			) // PK_EncryptorFilter
		); // StringSource
	}

	catch( CryptoPP::Exception& e )
	{
		cerr << "Caught Exception..." << endl;
		cerr << e.what() << endl;
	}

	return cipher;
}

/*
 *
 * name:	rsa_dec, RSA decryption using private key
 * @param	cipher
 * @return	deciphered result
 */

string rsa::rsa_dec(string cipher) const
{
	string recovered;
	try
	{
		AutoSeededRandomPool rng;
		RSAES_PKCS1v15_Decryptor d( privateKey );

		StringSource( cipher, true,
			new PK_DecryptorFilter( rng, d,
				new StringSink( recovered )
			) // PK_EncryptorFilter
		); // StringSource
    }
    catch( CryptoPP::Exception& e )
    {
	cerr << "Caught Exception..." << endl;
	cerr << e.what() << endl;
    }

	return recovered;
}

/*
 * name:	~rsa, class destructor
 * @param
 * @return
 */

rsa::~rsa() {}
