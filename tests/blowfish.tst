mode(-1)
cd ../samples
keyGen_blowfish("sampleKey", "sampleIV", "samplePublicKey", "samplePrivateKey")
// ECB mode
encryptFile_blowfish("sampleMatrix.sci", "samplePublicKey", "sampleKey", "0", "sampleIV", "cipher")
decryptFile_blowfish("cipher", "samplePrivateKey", "cipherKey", "0", "sampleIV", "recovered")
out = 0;
if chkFiles("sampleMatrix.sci", "recovered")
	disp("blowfish ECB: Success");
	out = out+1;
else
	disp("blowfish ECB: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// CBC mode
encryptFile_blowfish("sampleMatrix.sci", "samplePublicKey", "sampleKey", "1", "sampleIV", "cipher")
decryptFile_blowfish("cipher", "samplePrivateKey", "cipherKey", "1", "sampleIV", "recovered")
if chkFiles("sampleMatrix.sci", "recovered")
	disp("blowfish CBC: Success");
	out = out+1;
else
	disp("blowfish CBC: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// OFB mode
encryptFile_blowfish("sampleMatrix.sci", "samplePublicKey", "sampleKey", "2", "sampleIV", "cipher")
decryptFile_blowfish("cipher", "samplePrivateKey", "cipherKey", "2", "sampleIV", "recovered")
if chkFiles("sampleMatrix.sci", "recovered")
	disp("blowfish OFB: Success");
	out = out+1;
else
	disp("blowfish OFB: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// CFB mode
encryptFile_blowfish("sampleMatrix.sci", "samplePublicKey", "sampleKey", "3", "sampleIV", "cipher")
decryptFile_blowfish("cipher", "samplePrivateKey", "cipherKey", "3", "sampleIV", "recovered")
if chkFiles("sampleMatrix.sci", "recovered")
	disp("blowfish CFB: Success");
	out = out+1;
else
	disp("blowfish CFB: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// CTR mode
encryptFile_blowfish("sampleMatrix.sci", "samplePublicKey", "sampleKey", "4", "sampleIV", "cipher")
decryptFile_blowfish("cipher", "samplePrivateKey", "cipherKey", "4", "sampleIV", "recovered")
if chkFiles("sampleMatrix.sci", "recovered")
	disp("blowfish CTR: Success");
	out = out+1;
else
	disp("blowfish CTR: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// Conclusions
if out == 5
	disp("blowfish test: Success");
	blowfish = 1;
else
	disp("blowfish test: FAILURE");
	blowfish = 0;
end
deletefile("cipher");
deletefile("cipherKey");
deletefile("recovered");
cd ../tests
