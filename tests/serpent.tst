mode(-1)
cd ../samples
keyGen_serpent("sampleKey", "sampleIV", "samplePublicKey", "samplePrivateKey")
// ECB mode
encryptFile_serpent("sampleMatrix.sci", "samplePublicKey", "sampleKey", "0", "sampleIV", "cipher")
decryptFile_serpent("cipher", "samplePrivateKey", "cipherKey", "0", "sampleIV", "recovered")
out = 0;
if chkFiles("sampleMatrix.sci", "recovered")
	disp("serpent ECB: Success");
	out = out+1;
else
	disp("serpent ECB: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// CBC mode
encryptFile_serpent("sampleMatrix.sci", "samplePublicKey", "sampleKey", "1", "sampleIV", "cipher")
decryptFile_serpent("cipher", "samplePrivateKey", "cipherKey", "1", "sampleIV", "recovered")
if chkFiles("sampleMatrix.sci", "recovered")
	disp("serpent CBC: Success");
	out = out+1;
else
	disp("serpent CBC: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// OFB mode
encryptFile_serpent("sampleMatrix.sci", "samplePublicKey", "sampleKey", "2", "sampleIV", "cipher")
decryptFile_serpent("cipher", "samplePrivateKey", "cipherKey", "2", "sampleIV", "recovered")
if chkFiles("sampleMatrix.sci", "recovered")
	disp("serpent OFB: Success");
	out = out+1;
else
	disp("serpent OFB: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// CFB mode
encryptFile_serpent("sampleMatrix.sci", "samplePublicKey", "sampleKey", "3", "sampleIV", "cipher")
decryptFile_serpent("cipher", "samplePrivateKey", "cipherKey", "3", "sampleIV", "recovered")
if chkFiles("sampleMatrix.sci", "recovered")
	disp("serpent CFB: Success");
	out = out+1;
else
	disp("serpent CFB: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// CTR mode
encryptFile_serpent("sampleMatrix.sci", "samplePublicKey", "sampleKey", "4", "sampleIV", "cipher")
decryptFile_serpent("cipher", "samplePrivateKey", "cipherKey", "4", "sampleIV", "recovered")
if chkFiles("sampleMatrix.sci", "recovered")
	disp("serpent CTR: Success");
	out = out+1;
else
	disp("serpent CTR: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// Conclusions
if out == 5
	disp("serpent test: Success");
	serpent = 1;
else
	disp("serpent test: FAILURE");
	serpent = 0;
end
deletefile("cipher");
deletefile("cipherKey");
deletefile("recovered");
cd ../tests
